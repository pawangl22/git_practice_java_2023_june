
public class Calculator {
	
	static int add(int num1, int num2) {
		return num1+num2;
	}
	
	static int add(int num1, int num2, int num3) {
		return num1+num2+num3;
	}
	
	double add(double num1, double num2) {
		return num1+num2;
	}

	public static void main(String[] args) {
		
		int result = Calculator.add(10,30);
		System.out.println(result);
		
		System.out.println(Calculator.add(20, 30, 50));
		
		Calculator cal = new Calculator();
		double value = cal.add(10.06, 101.95);
		System.out.println(value);
	}
}
