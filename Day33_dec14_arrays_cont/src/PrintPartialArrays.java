
public class PrintPartialArrays {
	public static void main(String[] args) {
		int[] num = {10,20,30,40,50,60};
		System.out.println("Printing first half of the box");
		for (int i = 0; i < (num.length)/2; i++) {
			System.out.println(num[i]);
		}
		
		System.out.println("Printing second half of the box");
		for (int i = num.length-1; i > (num.length-1)/2; i--) {
			System.out.println(num[i]);
		}
	}

}
