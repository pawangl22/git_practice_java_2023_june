
public class PrintingArraysProgram {
	public static void main(String[] args) {
		int[] nums = {10,20,30,40,50,60};
		//Forward direction
		//Reverse direction
		//Forward - first half
		//Forward - second half
		//Reverse - first half
		//Reverse - second half
		
		System.out.println("Printing given values in forward direction");
		for (int num : nums) {
			System.out.println(num);
		}
		
		System.out.println("Printing given values in reverse direction");
		for (int i = nums.length-1; i >= 0; i--) {
			System.out.println(nums[i]);
		}
		
		System.out.println("Printing first half - forward direction");
		for (int i = 0; i < (nums.length)/2; i++) {
			System.out.println(nums[i]);
		}
		
		System.out.println("Printing second half - forward direction");
		for (int i = (nums.length)/2; i < nums.length; i++) {
			System.out.println(nums[i]);
		}
		
		System.out.println("Printing first half - reverse direction");
		for (int i = (nums.length-1)/2; i >= 0; i--) {
			System.out.println(nums[i]);
		}
		
		System.out.println("Printing seconf half - reverse direction");
		for (int i = nums.length-1; i >= (nums.length)/2; i--) {
			System.out.println(nums[i]);
		}
	}

}
