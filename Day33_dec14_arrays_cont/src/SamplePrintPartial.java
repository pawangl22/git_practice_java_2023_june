
public class SamplePrintPartial {

	public static void main(String[] args) {
		int[] nums = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
		
		System.out.println("Printing first half");
		for(int i=0; i <= (nums.length-1)/2; i++) {
			System.out.println(nums[i]);
		}
		
		System.out.println("Printing second half");
		for(int i=(nums.length)/2; i<=nums.length-1; i++)  {
			System.out.println(nums[i]);
		}
		
		System.out.println("Reverse printing");
		for(int i = nums.length-1; i>=0; i--) {
			System.out.println(nums[i]);
		}

		System.out.println("Forward printing");
		for(int i=0; i <= nums.length-1; i++) {
			System.out.println(nums[i]);
		}
	}

}
