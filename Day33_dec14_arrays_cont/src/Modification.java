
public class Modification {
	public static void main(String[] args) {
		int[] arr = { 10, 20, 30, 40, 50, 60 };
		for (int i = 0; i <= arr.length-1; i++) {
			arr[i] = arr[i] / 2;
			System.out.println(arr[i]);              // 5,10,15,20,25,30
		}
		System.out.println("Printing elements of an array");
		for (int i : arr) {
			System.out.println(i);
		}
	}

}
