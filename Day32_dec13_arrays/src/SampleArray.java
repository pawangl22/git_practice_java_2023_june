import java.util.Arrays;

public class SampleArray {

	public static void main(String[] args) {
		
		String[] cars = {"BMW", "AUDI", "Hyundai", "Benz"};
		int noOfCars = cars.length;
		System.out.println(noOfCars);
		
		System.out.println(Arrays.toString(cars));
		
		for( String car : cars) {
			System.out.println(car);
		}
		
		System.out.println(cars[3]);

	}

}
