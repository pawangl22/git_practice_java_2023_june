import java.util.Arrays;

public class VowelsExample {

	public static void main(String[] args) {

		char[] vowels = { 'A', 'E', 'I', 'O', 'U' };
		System.out.println(vowels[0]);    				//A
		System.out.println(vowels.length);				//5

		System.out.println(Arrays.toString(vowels));	//AEIOU

		for (char vowel : vowels) {
			System.out.println(vowel);

		}

	}

}
