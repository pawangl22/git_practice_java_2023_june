package com.mr.encapsulation.studentexample;

public class Student {
	private int id;
	private int age;
	private String name;
	private long phoneNo;

	public int getId() {
		return id;
	}

	public int getAge() {
		return age;
	}

	public String getName() {
		return name;
	}

	public long getPhoneNo() {
		return phoneNo;
	}

	public void setID(int id) {
		this.id = id;
	}

	public void setAge(int age) {
		if (age > 15 && age < 30)
			this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhoneNo(long phoneNo) {
		this.phoneNo = phoneNo;
	}

}
