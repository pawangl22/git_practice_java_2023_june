package com.mr.encapsulation.studentexample;

public class Runner {
	public static void main(String[] args) {

		Student s = new Student();
		s.setID(45);
		s.setName("Alpha");
		s.setAge(25);
		s.setPhoneNo(8879782209L);

		System.out.println(s.getAge());
		System.out.println(s.getName());
		System.out.println(s.getPhoneNo());
		System.out.println(s.getId());
	}

}
