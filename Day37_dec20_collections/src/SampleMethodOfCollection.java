import java.util.ArrayList;

public class SampleMethodOfCollection {
	public static void main(String[] args) {
		ArrayList list = new ArrayList();
		list.add(34);
		list.add("hello");
		list.add(4.5);
		list.add(true);
		list.add(null);
		list.add(34);
		System.out.println(list);
		System.out.println(list.size()); // 6
		System.out.println(list.isEmpty()); // false
		System.out.println(list.contains(4.5)); // true
		list.remove("hello");
		System.out.println(list);
		list.clear();
		System.out.println(list);
		
		
		ArrayList list1 = new ArrayList();
		list1.add(45);
		list1.add("GoodMorning");
		list1.add(7.5);
		list1.add(false);
		list1.add(null);
		list1.add(75);

	}

}
