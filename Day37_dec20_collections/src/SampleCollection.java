import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SampleCollection {
	public static void main(String[] args) {
		ArrayList<String> colorsList = new ArrayList<>();
		colorsList.add("Red");
		colorsList.add("Green");
		colorsList.add("Yellow");
		colorsList.add("Blue");
		colorsList.add("White");
		System.out.println(colorsList);

		// Printing elements in collection using for each loop
		for (String color : colorsList) {
			System.out.println(color);
		}
		
		//Sorting in ascending order
		Collections.sort(colorsList);
		System.out.println(colorsList);
		
		//Sorting in descending order
		Collections.sort(colorsList, Collections.reverseOrder());
		System.out.println(colorsList);
	}

}
