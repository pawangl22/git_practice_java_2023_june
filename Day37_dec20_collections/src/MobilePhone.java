import java.util.ArrayList;
import java.util.Collections;

public class MobilePhone {

	String color;
	String brand;
	double price;

	@Override
	public String toString() {
		return "MobilePhone [color=" + color + ", brand=" + brand + ", price=" + price + "]\n";
	}

	MobilePhone(String color, String brand, double price) {
		this.color = color;
		this.brand = brand;
		this.price = price;
	}

	public static void main(String[] args) {
		MobilePhone m1 = new MobilePhone("Black", "Google", 45000);
		MobilePhone m2 = new MobilePhone("White", "Samsung", 65000);
		MobilePhone m3 = new MobilePhone("Gray", "Apple", 145000);
		MobilePhone m4 = new MobilePhone("Blue", "Vivo", 25000);

		// Create a collection that holds only Mobile objects and add all the mobile
		ArrayList<MobilePhone> mobileList = new ArrayList<>();
		mobileList.add(m1);
		mobileList.add(m2);
		mobileList.add(m3);
		mobileList.add(m4);

		System.out.println(mobileList);

		// foreach loop
		for (MobilePhone mobile : mobileList) {
			System.out.println(mobile);
		}

	}

}
