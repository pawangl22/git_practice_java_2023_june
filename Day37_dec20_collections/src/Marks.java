import java.util.ArrayList;
import java.util.Collections;

public class Marks {
	int maths;
	int science;
	int social;

	Marks(int maths, int science, int social) {
		this.maths = maths;
		this.science = science;
		this.social = social;
	}

	@Override
	public String toString() {
		return "Marks=[maths=" + maths + ", science=" + science + ", social=" + social + "]\n";
	}
	
	public static void main(String[] args) {
		Marks m1 = new Marks(87, 40, 60);
		Marks m2 = new Marks(98, 78, 99);
		Marks m3 = new Marks(56, 78, 88);
		
		ArrayList<Marks> marksList = new ArrayList<>();
		marksList.add(m1);
		marksList.add(m2);
		marksList.add(m3);
		
		System.out.println(marksList);
		
		//sort
		
	}

}
