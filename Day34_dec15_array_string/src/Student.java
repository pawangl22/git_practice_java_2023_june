import java.util.Arrays;

public class Student {
	int slNo;
	String name;
	int age;

	Student(int slNo, String name, int age) {
		this.slNo = slNo;
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Student [slNo=" + slNo + ", name=" + name + ", age=" + age + "]";
	}

	public static void main(String[] args) {
		Student s1 = new Student(1, "Alpha", 30);
		Student s2 = new Student(2, "Beta", 35);
		Student s3 = new Student(3, "Gama", 40);

		Student[] students = { s1, s2, s3 };
		System.out.println(students);
		System.out.println(students[0]);
		System.out.println(students[1]);
		System.out.println(students[2]);
		students[0].age = 25;
		System.out.println(s1.age);
		students[2].slNo = 50;
		System.out.println(s3.slNo);
		System.out.println("_____________________________________\n");

		System.out.println("****** Print all the elements using Array.toString ******\n");
		System.out.println(Arrays.toString(students));
		System.out.println("______________________________________\n");

		System.out.println("****** Print all the elements using for each loop ******\n");
		for (Student student : students) {
			System.out.println(student);
			System.out.println(student.name);
			System.out.println(student.age + 2);
		}

	}

}
