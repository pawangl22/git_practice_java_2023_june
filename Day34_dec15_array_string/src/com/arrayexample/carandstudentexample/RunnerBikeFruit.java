package com.arrayexample.carandstudentexample;

public class RunnerBikeFruit {

	public static void main(String[] args) {

		Bike bike = new Bike("BMW", 2500000.00, 500, "Black");
		Fruits fruit = new Fruits(345.99, 45, "Yellow");
		
		Object[] obj = {bike, fruit};
		
		for( Object object : obj) {

			if( object instanceof Fruits f ) {
				System.out.println(f.quantity);
			}else if (object instanceof Bike b) {
				System.out.println(b.brand);
			}
		}
		
		

	}

}
