package com.arrayexample.carandstudentexample;

public class Student {
	int slNo;
	String name;
	int age;

	Student(int slNo, String name, int age) {
		this.slNo = slNo;
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Student [slNo=" + slNo + ", name=" + name + ", age=" + age + "]";
	}

}
