package com.arrayexample.carandstudentexample;

public class Car {
	String regNo;
	String color;

	Car(String regNo, String color) {
		this.regNo = regNo;
		this.color = color;
	}

	@Override
	public String toString() {
		return "Car [regNo=" + regNo + ", color=" + color + "]";
	}

}
