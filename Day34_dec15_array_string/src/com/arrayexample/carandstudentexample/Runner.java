package com.arrayexample.carandstudentexample;

public class Runner {
	public static void main(String[] args) {

		Car c1 = new Car("KA05 NE6958", "White");
		Student s1 = new Student(1, "Alpha", 25);

		Object[] obj = {s1, c1};

		for (Object object : obj) {
			
			if (object instanceof Student s) {
				System.out.println(s);
			} else if (object instanceof Car c) {
				System.out.println(c.color);
			}
		}
	}
}