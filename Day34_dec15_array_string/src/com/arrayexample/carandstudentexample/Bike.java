package com.arrayexample.carandstudentexample;

public class Bike {

	String brand;
	double price;
	int engineCC;
	String color;

	public Bike(String brand, double price, int engineCC, String color) {
		this.brand = brand;
		this.price = price;
		this.engineCC = engineCC;
		this.color = color;
	}

	@Override
	public String toString() {
		return "Bike [brand=" + brand + ", price=" + price + ", engineCC=" + engineCC + ", color=" + color + "]";
	}

}
