package com.arrayexample.carandstudentexample;

public class Fruits {
	
	double price;
	int quantity;
	String color;
	
	public Fruits(double price, int quantity, String color) {
		this.price = price;
		this.quantity = quantity;
		this.color = color;
	}
	
	@Override
	public String toString() {
		return "Fruits [price=" + price + ", quantity=" + quantity + ", color=" + color + "]";
	}
	
	

}
