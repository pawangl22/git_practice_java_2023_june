package automobileexample;

public class Shop {

	Vehicle sell(int option) {

		if (option == 1) {
			Car c = new Car();
			c.color = "White";
			c.brand = "AUDI";
			c.price = 6500000;
			return c;
		} else if (option == 2) {
			Bike b = new Bike();
			b.color = "Black";
			b.brand = "KTM";
			b.inKickerAvailable = false;
			b.price = 450000;
			return b;
		} else {
			Truck t = new Truck();
			t.color = "Red";
			t.brand = "Bharath Benz";
			t.price = 100000000;
			t.dropLoad();
			t.loadCapacity=5000;
			return t;
		}
	}

	public static void main(String[] args) {
		Shop s = new Shop();

		Vehicle v = s.sell(1); // Vehicle v = new Car();
		System.out.println(v);
		v.start();
		v.stop();

		if (v instanceof Car c) { //Downcasting
			System.out.println(c.isAutomatic = false);
			c.cruizeControl();
		} else if (v instanceof Bike) {
			Bike b = (Bike) v; // Downcasting
			System.out.println(b.inKickerAvailable = false);
			b.kick();
		} else if (v instanceof Truck) {
			Truck t = (Truck) v;
			t.dropLoad();
			System.out.println(t.loadCapacity);
		}
	}

}
