package automobileexample;

public class ShowRoom {

	Car sellCar() {
		Car c = new Car();
		c.color = "White";
		c.brand = "BMW";
		c.isAutomatic = true;
		c.price = 6700000;
		c.cruizeControl();
		return c;

	}

	public static void main(String[] args) {

		ShowRoom sr = new ShowRoom();
		Car myCar = sr.sellCar();
		System.out.println(myCar);

	}

}
