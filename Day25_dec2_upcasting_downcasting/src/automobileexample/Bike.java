package automobileexample;

public class Bike extends Vehicle {
	
	boolean inKickerAvailable;
	
	void kick() {
		System.out.println("Kick start");
	}

	@Override
	public String toString() {
		return "Bike [color=" + color + ", brand=" + brand + ", price=" + price + ", inKickerAvailable="
				+ inKickerAvailable + "]";
	}

}
