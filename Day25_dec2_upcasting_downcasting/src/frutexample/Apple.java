package frutexample;

public class Apple extends Fruit {
	boolean isSeedless;

	Apple(String color) {
		super(color);
	}

	@Override
	public String toString() {
		return "Apple [isSeedless=" + isSeedless + ", color=" + color + "]";
	}

}
