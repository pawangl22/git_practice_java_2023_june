package frutexample;

public class Guava extends Fruit {

	boolean isSeedless;

	Guava(String color) {
		super(color);

	}

	@Override
	public String toString() {
		return "Guava [isSeedless=" + isSeedless + ", color=" + color +"]";
	}
}
