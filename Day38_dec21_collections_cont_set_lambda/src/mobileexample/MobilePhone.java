package mobileexample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class MobilePhone implements Comparable<MobilePhone> {

	String color;
	String brand;
	double price;

	@Override
	public String toString() {
		return "MobilePhone [color=" + color + ", brand=" + brand + ", price=" + price + "]\n";
	}

	MobilePhone(String color, String brand, double price) {
		this.color = color;
		this.brand = brand;
		this.price = price;
	}

	public static void main(String[] args) {
		MobilePhone m1 = new MobilePhone("Black", "Google", 45000);
		MobilePhone m2 = new MobilePhone("White", "Samsung", 65000);
		MobilePhone m3 = new MobilePhone("Gray", "Apple", 145000);
		MobilePhone m4 = new MobilePhone("Blue", "Vivo", 25000);

		// Create a collection that holds only Mobile objects and add all the mobile in
		ArrayList<MobilePhone> mobileList = new ArrayList<>(Arrays.asList(m1, m2, m3, m4));

		// for each loop
		for (MobilePhone mobile : mobileList) {
			System.out.println(mobile);
		}

		// sorting with brand - ascending
		System.out.println("**** Sorting based on brand - Ascending ****\n");
		Collections.sort(mobileList);
		for (MobilePhone mobile : mobileList) {
			System.out.println(mobile);
		}

		// sorting with price - Ascending usng Lambda expressions
		System.out.println("**** Sorting based on price - Ascending using Lambda expression ****\n");
		Comparator<MobilePhone> mPrice = (mp1, mp2) -> (int) (mp1.price - mp2.price);
		Collections.sort(mobileList, mPrice);
		for (MobilePhone mobile : mobileList) {
			System.out.println(mobile);
		}

		// sorting with price - Ascending usng Lambda expressions
		System.out.println("**** Sorting based on price - Descending using Lambda expression ****\n");
		Collections.sort(mobileList, mPrice.reversed());
		for (MobilePhone mobile : mobileList) {
			System.out.println(mobile);
		}

		// sorting with Color - Ascending using Lambda expressions
		System.out.println("**** Sorting based on color - Ascending using Lambda expression ****\n");
		Comparator<MobilePhone> mColor = (mc1, mc2) -> mc1.color.compareTo(mc2.color);
		Collections.sort(mobileList, mColor);
		for (MobilePhone mobile : mobileList) {
			System.out.println(mobile);
		}

		// sorting with Color - Descending using Lambda expressions
		System.out.println("**** Sorting based on color - Descending using Lambda expression ****\n");
		Collections.sort(mobileList, mColor.reversed());
		for (MobilePhone mobile : mobileList) {
			System.out.println(mobile);
		}

	}

	@Override
	public int compareTo(MobilePhone o) {
		return this.brand.compareTo(o.brand);
	}

}
