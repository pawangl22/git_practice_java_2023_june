package bookexample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Book implements Comparable<Book> {
	double price;
	String brand;
	int noOfPages;
	long sNo;

	Book(double price, String brand, int noOfPages, long sNo) {
		this.price = price;
		this.brand = brand;
		this.noOfPages = noOfPages;
		this.sNo = sNo;
	}

	@Override
	public String toString() {
		return "Book [price=" + price + ", brand=" + brand + ", noOfPages=" + noOfPages + ", sNo=" + sNo + "]\n";
	}

	public static void main(String[] args) {
		Book b1 = new Book(150, "Alpha", 1365, 1234567L);
		Book b2 = new Book(250, "Lion", 465, 8909889L);
		Book b3 = new Book(350, "King", 5265, 1567898L);
		Book b4 = new Book(350, "Blue", 565, 6789019L);

		ArrayList<Book> booksList = new ArrayList<>();
		booksList.add(b1);
		booksList.add(b2);
		booksList.add(b3);

		System.out.println(booksList);

		// sorting the book with brand
		System.out.println("Sorting based on brand name - Ascending order");
		Collections.sort(booksList);
		System.out.println(booksList);

		// sorting the book with price - Ascending
		System.out.println("Sorting based on price - Ascending order");
		PriceComparator pc = new PriceComparator();
		Collections.sort(booksList, pc);
		System.out.println(booksList);

		// sorting the book with price - Descending
		System.out.println("Sorting based on price - Descending order");
		Collections.sort(booksList, pc.reversed());
		System.out.println(booksList);

		// sorting the book with NoOfPage - Ascending
		System.out.println("Sorting based on No of page - Ascending order");
		NoOfPageComparator pageNo = new NoOfPageComparator();
		Collections.sort(booksList, pageNo);
		System.out.println(booksList);

		// sorting the book with NoOfPage - Descending
		System.out.println("Sorting based on No of page - Descending order");
		Collections.sort(booksList, pageNo.reversed());
		System.out.println(booksList);
		
		//Sorting the book with serial no. using lambda expression
		System.out.println("Sorting based on serial no. using Lambda expresion - Ascending order");
		Comparator<Book> serialNo = (x, y)-> (int) (x.sNo - y.sNo);
		Collections.sort(booksList, serialNo);
		System.out.println(booksList);
		
		//Sorting the book with serial no. using lambda expression - Descending
		System.out.println("Sorting based on serial no. using Lambda expresion - Descending order");
		Collections.sort(booksList, serialNo.reversed());
		System.out.println(booksList);
	}

	@Override
	public int compareTo(Book o) {
		return this.brand.compareTo(o.brand);
	}

}
