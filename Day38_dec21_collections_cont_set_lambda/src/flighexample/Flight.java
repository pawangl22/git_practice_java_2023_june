package flighexample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Flight implements Comparable<Flight> {

	int flightNo;
	int noOfSeats;
	String flightCarrierName;
	double costPerTicket;

	Flight(int flightNo, int noOfSeats, String flightCarrierName, double costPerTicket) {
		this.flightNo = flightNo;
		this.noOfSeats = noOfSeats;
		this.flightCarrierName = flightCarrierName;
		this.costPerTicket = costPerTicket;
	}

	@Override
	public String toString() {
		return "Flight [flightNo=" + flightNo + ", noOfSeats=" + noOfSeats + ", flightCarrierName=" + flightCarrierName
				+ ", costPerTicket=" + costPerTicket + "]";
	}

	public static void main(String[] args) {
		Flight f1 = new Flight(705, 250, "Air Asia", 7500.00);
		Flight f2 = new Flight(810, 350, "Qatar Airways", 11500.00);
		Flight f3 = new Flight(245, 150, "Indigo Airways", 2500.00);
		Flight f4 = new Flight(905, 400, "SriLanka Airlines", 3500.00);
		Flight f5 = new Flight(160, 8000, "Dubai Airine", 77500.00);

		ArrayList<Flight> flightList = new ArrayList<>(Arrays.asList(f1, f2, f3, f4, f5));

		// Sorting the flight based on flight No
		System.out.println("\n **** Sorting the flight based on flight no - Default sorting ****\n");
		Collections.sort(flightList);
		for (Flight airFlight : flightList) {
			System.out.println(airFlight);
		}

		// Sorting the flight based on cost per ticket using comparator
		System.out.println("\n **** Sorting the flight based on cost per ticket - using comparator ****\n");
		Comparator<Flight> ticketCost = (tc1, tc2) -> (int) (tc1.costPerTicket - tc2.costPerTicket);
		Collections.sort(flightList, ticketCost);
		for (Flight airFlight : flightList) {
			System.out.println(airFlight);
		}

	}

	@Override
	public int compareTo(Flight o) {
		return (this.flightNo - o.flightNo);
	}

}
