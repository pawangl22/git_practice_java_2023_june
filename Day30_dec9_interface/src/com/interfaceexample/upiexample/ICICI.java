package com.interfaceexample.upiexample;

public class ICICI implements UPI {

	@Override
	public void sendMoney() {
		System.out.println("Sending money from ICICI to other bank");

	}

	@Override
	public void receivedMoney() {
		System.out.println("Receving money from other bank to ICICI account");

	}
	
	@Override
	public void checkBalance() {
		System.out.println("Checking balance in your ICICI account");
	}

}
