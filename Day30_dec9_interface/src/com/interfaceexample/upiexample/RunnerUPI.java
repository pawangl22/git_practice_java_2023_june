package com.interfaceexample.upiexample;

public class RunnerUPI {
	public static void main(String[] args) {
		
		UPI u = new HDFC();
		u.sendMoney();
		u.receivedMoney();
		u.checkBalance();
		
		if (u instanceof HDFC h) {
			h.sendMoneyToMobile();
		}
	}

}
