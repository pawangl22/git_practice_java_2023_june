package com.interfaceexample.upiexample;

public interface UPI {

	void sendMoney();

	void receivedMoney();
	
	void checkBalance();
}
