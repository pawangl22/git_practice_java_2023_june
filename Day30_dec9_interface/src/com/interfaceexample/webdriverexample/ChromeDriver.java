package com.interfaceexample.webdriverexample;

public class ChromeDriver implements WebDriver {

	@Override
	public void get(String url) {
		System.out.println("Opening " + url +" in chrome browser");

	}

	@Override
	public void openPrivate() {
		System.out.println("Opening incognito window");

	}

}
