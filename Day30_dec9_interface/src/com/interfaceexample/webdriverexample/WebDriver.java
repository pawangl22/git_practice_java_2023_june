package com.interfaceexample.webdriverexample;

public interface WebDriver {
	
	//By default public abstract will be there in interface
	void get(String url);

	void openPrivate();
}
