
public class ReverseStringBuiderExample {
	public static void main(String[] args) {
		String message = "Hello World";
		StringBuilder sb = new StringBuilder(message);
		sb.reverse();
		String reveString = sb.toString();
		System.out.println(reveString);
		
		System.out.println(new StringBuilder(message).reverse()); //in one line
	}

}
