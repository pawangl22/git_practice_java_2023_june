package busexample;

public class Bus {

	String serviceProvider;
	int seatCapacity;
	String source;
	String destination;
	double tickeCost;

	Bus(String serviceProvider, int seatCapacity, String source, String destination, double tickeCost) {
		this.serviceProvider = serviceProvider;
		this.seatCapacity = seatCapacity;
		this.source = source;
		this.destination = destination;
		this.tickeCost = tickeCost;
	}

	public String toString() {
		return "Bus to " + this.destination;
	}

	public static void main(String[] args) {
		Bus b1 = new Bus("Orange", 30, "Bengaluru", "Goa", 4000);
		Bus b2 = new Bus("VRL", 34, "Bengaluru", "Mangalore", 800);

		System.out.println(b1);
		System.out.println(b2);

	}

}
