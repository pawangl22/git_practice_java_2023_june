package carexample;

public class Car {
	void start() {
		System.out.println("Turn the key to start");
	}

	public static void main(String[] args) {

		SedanCar s = new SedanCar();
		s.start();

		SuperCar sc = new SuperCar();
		sc.start();
	}
}
