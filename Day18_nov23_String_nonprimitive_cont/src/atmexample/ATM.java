package atmexample;

public class ATM {

	Money withdraw(Card card, int pin) {
		System.out.println("ATM accepted card with details " + card);
		Money m = new Money(1000);
		return m;
	}

	public static void main(String[] args) {
		ATM a = new ATM();
		Card c = new Card(897656789812L, "Alpha", 345);
		Money paisa = a.withdraw(c, 9989);
		System.out.println(paisa);
	}
}
