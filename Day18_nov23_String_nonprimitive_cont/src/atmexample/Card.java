package atmexample;

public class Card {
	long cardNo;
	String cardHolderName;
	int cvv;
	
	Card(long cardNo, String cardHolderName, int CVV){
		this.cardNo=cardNo;
		this.cardHolderName=cardHolderName;
		this.cvv=cvv;
	}

	@Override
	public String toString() {
		return "Card [cardNo=" + cardNo + ", cardHolderName=" + cardHolderName + ", cvv=" + cvv + "]";
	}
	
	
}
