package studentexample;

public class Student {

	String name;
	int age;
	long phoneNumber;

	Student(String name, int age, long phoneNumber) {
		this.name = name;
		this.age = age;
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", phoneNumber=" + phoneNumber + "]";
	}

}
