package studentexample;

public class EligibilityChecker {

	void printEligibilityToVote(Student s) {
		System.out.println(s.age >= 18 ? "Eligible to vote" : "Not eligible to vote");
		System.out.println("Student details are mentioned below\n");
		System.out.println(s);

	}

	public static void main(String[] args) {

		EligibilityChecker eCheck = new EligibilityChecker();
		Student st = new Student("Pawan", 18, 8879782209L);
		eCheck.printEligibilityToVote(st);
	}

}
