package tokenexample;

public class TokenGenerator {

	Token generateToken() {
		Token t = new Token(22);
		return t;
	}

	public static void main(String[] args) {
		TokenGenerator machine = new TokenGenerator();
		Token tokenChit = machine.generateToken();
		System.out.println(tokenChit);
	}

}
