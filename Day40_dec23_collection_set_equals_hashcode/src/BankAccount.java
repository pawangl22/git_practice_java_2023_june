import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public class BankAccount {

	@Override
	public int hashCode() {
		return Objects.hash(accountNo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		return accountNo == other.accountNo;
	}

	long accountNo;
	String name;
	double balance;
	
	BankAccount(long accountNo, String name, double balance) {
		this.accountNo = accountNo;
		this.name = name;
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "BankAccount [accountNo=" + accountNo + ", name=" + name + ", balance=" + balance + "]\n";
	}
	
	public static void main(String[] args) {
		BankAccount b1 = new BankAccount(6789543, "Alpha", 7897.0);
		BankAccount b2 = new BankAccount(6789543, "Beta", 678000.0);
		BankAccount b3 = new BankAccount(6789543, "Charlie", 5678.0);
		BankAccount b4 = new BankAccount(5890925, "Charlie", 5678.0);
	
		
		HashSet<BankAccount> set = new HashSet<>(Arrays.asList(b1, b2, b3, b4));
		System.out.println(set);
	}
	
}
