import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public class Order {
	int orderID;
	double totalCost;
	int quantity;
	
	Order(int orderID, double totalCost, int quantity) {
		this.orderID = orderID;
		this.totalCost = totalCost;
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Order [orderID=" + orderID + ", totalCost=" + totalCost + ", quantity=" + quantity + "]\n";
	}
	
	public static void main(String[] args) {
		Order o1 = new Order(456, 789.68, 5);
		Order o2 = new Order(456, 755.00, 5);
		Order o3 = new Order(457, 322.97, 2);
		Order o4 = new Order(456, 89, 4);
		Order o5 = new Order(345, 1789.65, 10);
		
		HashSet<Order> orderSet = new HashSet<>(Arrays.asList(o1, o2, o3, o4, o5));
		//System.out.println(orderSet);
		for (Order order : orderSet) {
			System.out.println(order);
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(orderID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		return orderID == other.orderID;
	}
	
}
