import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class SampleTreeSet {
	public static void main(String[] args) {
		
		TreeSet<Integer> number = new TreeSet<>(Arrays.asList(1,6,7,4,2,10,3,5,9,8));
		System.out.println(number);
		
		TreeSet<String> names = new TreeSet<>(Collections.reverseOrder());
		names.addAll(Arrays.asList("Pawan", "Kumar", "Alpha", "Beta", "Zara"));
		System.out.println(names);
	}

}
