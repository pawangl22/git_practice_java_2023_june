
public class Unboxing {
	public static void main(String[] args) {
		String name="25";
		System.out.println(name);
		
		String n = name;
		System.out.println(name.toUpperCase());
		System.out.println(name.isEmpty());
		System.out.println(name.concat(" Vanitha"));
		String[] n1 = name.split(name);
		System.out.println(n1.toString());
		
		int age = Integer.parseInt(name);
		System.out.println(age+2);
	}

}
