import java.util.ArrayList;

public class Autoboxing {
	public static void main(String[] args) {
		int x=50;
		char c='B';
		Integer i=x;
		System.out.println(i);
		
		
		Integer j = Integer.valueOf(x);
		System.out.println(j);
		
		Integer c1=(int) c;
		System.out.println(c1);
		
		
	}

}
