package com.trai.simexample;

public class JioSim extends AbstractSim {

	@Override
	public void call() {
		System.out.println("Jio - Calling...");

	}

	@Override
	public void sms() {
		System.out.println("Jio - Sending sms...");

	}

	void JioTV() {
		System.out.println("JIO TV...");
	}

}
