package com.trai.simexample;

public class AirtelSim extends AbstractSim {

	@Override
	public void call() {
		System.out.println("Airtel - Calling....");

	}

	@Override
	public void sms() {
		System.out.println("Airtel - Sending sms...");

	}

	@Override
	public void sos() {
		System.out.println("Airtel - Emergency calling 109...");
	}

}