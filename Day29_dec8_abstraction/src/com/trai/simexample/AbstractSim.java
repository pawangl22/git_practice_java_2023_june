package com.trai.simexample;

public abstract class AbstractSim {

	public abstract void call();

	public abstract void sms();

	public void sos() {
		System.out.println("Emergency calling 112");
	}

}
