package com.trai.simexample;

public class RunnerSim {
	public static void main(String[] args) {

		AbstractSim a = new JioSim(); // upcasting
		a.call();
		a.sms();
		a.sos();

		// to access the subclass specific method
		// we have to downcast
		// to avoid ClassCastException we use instanceof operator
		if (a instanceof JioSim) { // (a instanceof JioSim j)
			JioSim j = (JioSim) a;
			j.JioTV();

		}

	}

}
