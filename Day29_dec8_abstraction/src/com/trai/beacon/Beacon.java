package com.trai.beacon;

public class Beacon extends AbstractBeacon {

	@Override
	public void sendSignal() {
		System.out.println("Sending signal to Navigator");

	}

	void turnOn() {
		System.out.println("Turning on Beacon device");
	}

	void turnOff() {
		System.out.println("Turning off Beacon device");
	}

}
