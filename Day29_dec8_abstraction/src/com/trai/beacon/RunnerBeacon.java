package com.trai.beacon;

public class RunnerBeacon {

	public static void main(String[] args) {

		AbstractBeacon a = new Beacon();

		if (a instanceof Beacon) {
			Beacon b = (Beacon) a;
			b.turnOn();
			b.sendSignal();
			b.turnOff();

		}
	}

	
}
