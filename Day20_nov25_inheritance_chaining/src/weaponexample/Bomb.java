package weaponexample;

public class Bomb extends Weapon {

	Bomb() {
		super("Green");
	}

	@Override
	public String toString() {
		return "Bomb [color=" + color + "]";
	}

}
