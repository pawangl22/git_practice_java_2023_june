package weaponexample;

public class RunnerWeapon {
	
	public static void main(String[] args) {
		
		System.out.println("***** Calling Gun Object *****");
		Gun gun = new Gun("Black");
		System.out.println(gun);
		gun.clean();

		System.out.println("\n***** Calling Bomb Object *****");
		Bomb bomb = new Bomb();
		System.out.println(bomb);
		bomb.clean();
	}

}
