package weaponexample;

public class Weapon {

	String color;

	void clean() {
		System.out.println("Cleaning weapon");
	}

	Weapon(String color) {
		this.color = color;
	}

}
