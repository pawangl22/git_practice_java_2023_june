package fruitexample;

public class Apple extends Fruit {
	boolean isSeedless;

	Apple(String color){
		super(color);
	}

	Apple() {
		super("BLUE");
	}

	public static void main(String[] args) {
		Apple a1 = new Apple("RED");
		System.out.println(a1);
		
		Apple a2 = new Apple();
		System.out.println(a2);
	}
}
