
public class Address {
	int houseNo;
	String city;
	String streetNo;
	int pinCode;
	
	@Override
	public String toString() {
		return "Address [houseNo=" + houseNo + ", city=" + city + ", streetNo=" + streetNo + ", pinCode=" + pinCode
				+ "]";
	}
	
	
}
