
public class Student {

	int sNo;
	String name;
	Address address = new Address();
	
	@Override
	public String toString() {
		return "Student [sNo=" + sNo + ", name=" + name + ", address=" + address + "]";
	}	
	
	public static void main(String[] args) {
		Student s = new Student();
		System.out.println(s);
		s.sNo=5;
		s.name="Alpha";
		s.address.houseNo=132;
		s.address.city="Bengaluru";
		s.address.streetNo="4th Cross";
		s.address.pinCode=560085;
		System.out.println(s);
		
	}
	
}
