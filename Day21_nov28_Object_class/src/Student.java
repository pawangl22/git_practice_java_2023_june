
public class Student {
	String name;
	int age;
	static String collegeName = "XYZ";

	public static void main(String[] args) {
		System.out.println(Student.collegeName); // XYZ
		Student s1 = new Student();
		Student s2 = new Student();
		System.out.println(s1.collegeName); // XYZ
		System.out.println(s2.collegeName); // XYZ

		Student.collegeName = "ABC";
		System.out.println(s1.collegeName); // ABC
		System.out.println(s2.collegeName); // ABC
		System.out.println(Student.collegeName); // ABC

	}

}
