package practice;

public class PracticeWhileLoop {

	// Sum of 1 to N

	public static void main(String[] args) {

		int i = 1;
		int n = 10;
		int sum = 0;

		while (i <= n) {
			sum += i;
			i++;
		}
		System.out.println(sum);
	}

}
