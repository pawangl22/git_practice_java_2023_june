package practice;

import java.util.Scanner;

public class PracticeScannerExample {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Please enter a number");
		int x = s.nextInt();
		System.out.println(x);
		
		System.out.println("Please enter name");
		String name = s.next();
		System.out.println(name);
	}
}
