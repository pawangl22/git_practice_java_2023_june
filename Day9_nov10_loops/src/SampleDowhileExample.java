import java.util.Scanner;

public class SampleDowhileExample {

	public static void main(String[] args) {
		//Creating scanner object outside the loop
		Scanner s = new Scanner(System.in);

		do {
			System.out.println("Enter Two numbers");

			int num1 = s.nextInt();
			int num2 = s.nextInt();
			int sum = num1 + num2;

			System.out.println(sum);
			System.out.println("Do you want to add more number? Type true/false");
		} while (s.nextBoolean());
		//When the user enter true, the loop executes again
		//When the user enter false, the program terminates
	}
}
