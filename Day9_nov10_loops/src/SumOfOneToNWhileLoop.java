
public class SumOfOneToNWhileLoop {

	//Print sum of 1 to n values
	public static void main(String[] args) {
		int i=1;
		int n=10;
		int sum=0;
		
		while(i<=n) {
			sum+=i;
			i++;
		}
		System.out.println("Total sum of I and N is "+sum);
	}
}
