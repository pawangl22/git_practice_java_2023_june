import java.util.Scanner;

public class SampleScannerExample {

	public static void main(String[] args) {
		
		Scanner userInput = new Scanner(System.in);
		System.out.println("Enter a number");
		int x = userInput.nextInt();
		System.err.println("User given number is "+x);
		
		//It will accept on word input
		String message = userInput.next();
		System.out.println("This message is "+message);
	}
}
