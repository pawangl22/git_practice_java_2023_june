package strings;


import java.text.SimpleDateFormat;
import java.util.Date;

public class SampleDateProgram {
	
	public static void main(String[] args) throws Exception {
		String sDate1 = "01-Jun-2023";
		Date date1 = new SimpleDateFormat("DD-MMM-YYYY").parse(sDate1);
		System.out.println(date1);
	}
}
