package condition;

public class PracticeSwitchString {

	public static void main(String[] args) {

		String formType = "logout";

		switch (formType) {

		case "login":
			System.out.println("User Logged in");
			break;
		case "Logout":
			System.out.println("User Logged out");
			break;
		case "Profile":
			System.out.println("Profile page");
			break;
		default:
			System.out.println("Invalid page");

		}

	}

}
