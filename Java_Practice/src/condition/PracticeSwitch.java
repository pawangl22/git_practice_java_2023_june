package condition;

public class PracticeSwitch {
	public static void main(String[] args) {
		char c = 'l';
		
		switch(c) {
		
		case 'y':
		case 'Y':
				System.out.println("YES");
				break;
		
		case 'n', 'N':
				System.out.println("NO");
		
		default: System.out.println("not a valid character");
		}
	}

}
