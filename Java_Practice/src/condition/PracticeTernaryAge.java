package condition;

public class PracticeTernaryAge {
	public static void main(String[] args) {

		int age = 102;

		System.out.println((age >= 0 && age <= 11) ? "Child" : (age > 11) && (age <= 100) ? "Adult" : "invalid");
	}

}
