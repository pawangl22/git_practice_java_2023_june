package condition;

public class PracticeElseIf {

	public static void main(String[] args) {
		String formType = "profiles";

		if (formType.equalsIgnoreCase("login")) {
			System.out.println("User Logged in");
		} else if (formType.equalsIgnoreCase("Logout")) {
			System.out.println("User Logged out");
		} else if (formType.equalsIgnoreCase("ProfiLE")) {
			System.out.println("Profile page");
		} else {
			System.out.println("Invalid page");
		}
	}

}
