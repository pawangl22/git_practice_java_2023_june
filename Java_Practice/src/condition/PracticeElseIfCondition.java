package condition;

public class PracticeElseIfCondition {

	public static void main(String[] args) {

		int sub1 = 24;
		int sub2 = 35;

		if (sub1 >= 65 && sub2 >= 75) {
			System.out.println("D");
		} else if (sub1 >= 45 && sub2 <= 64) {
			System.out.println("F");
		} else {
			System.out.println("S");
		}
	}
}
