package condition;

public class PracticeTernaryOperator {
	public static void main(String[] args) {

		int i = 19;
		System.out.println((i % 2 == 0) ? "Even" : "Odd");

	}
}
