package programming;

import java.util.Scanner;

public class SimpleDigitCheck {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter the value");
		int n = scan.nextInt();
		scan.close();

		if (n >= 10 && n <= 99) {
			System.out.println("It is a two digit no.");
		} else {
			System.out.println("It is not a two digit no");
		}
	}

}
