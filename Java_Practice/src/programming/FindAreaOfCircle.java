package programming;

import java.util.Scanner;

public class FindAreaOfCircle {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter the value");
		double radius = scan.nextDouble();
		scan.close();
		
		double pi = 3.142;
		
		double areaOfCircle = pi * radius * radius;
		System.out.println(areaOfCircle);
		
		
	}

}
