package programming;

import java.util.Scanner;

public class StarL {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter n value");
		int n = scan.nextInt();
		scan.close();

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (j == 0 || (i == n - 1 && j <= n / 2))
					System.out.print("*");
				else
					System.out.print(" ");
			}
			System.out.println();

		}
	}

}
