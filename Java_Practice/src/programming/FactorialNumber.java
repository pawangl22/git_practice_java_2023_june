package programming;

public class FactorialNumber {
	public static void main(String[] args) {
		
		int result=1;
		int n=5;
		
		for (int i = n; i >=2; i--) {
			result = result*i;
		}
		System.out.println(result);
	}

}
