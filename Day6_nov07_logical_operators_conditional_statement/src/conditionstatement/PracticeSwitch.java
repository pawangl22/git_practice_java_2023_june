package conditionstatement;

public class PracticeSwitch {

	public static void main(String[] args) {

		int x = 1;

		switch (x) {

		case 1:
			System.out.println("ONE");
		case 2:
			System.out.println("TWO");
		case 3:
			System.out.println("THREE");
			break;
		case 4:
			System.out.println("FOUR");
		}

	}
}
