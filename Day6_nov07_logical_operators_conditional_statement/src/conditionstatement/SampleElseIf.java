package conditionstatement;

public class SampleElseIf {

	public static void main(String[] args) {

		int marks = 75;
		int avg = 90;

		if (marks >= 75 && avg <= 80) {
			System.out.println("Student1");
			
		} else if (marks >= 71 && avg <= 65) {
			System.out.println("Student2");
			
		} else {
			System.out.println("Not applicable");
		}
	}
}
