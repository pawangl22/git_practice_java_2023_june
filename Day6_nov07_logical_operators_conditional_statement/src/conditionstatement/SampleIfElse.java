package conditionstatement;

public class SampleIfElse {

	public static void main(String[] args) {

		int age = 19;
		char gender = 'M';

		if (age > 18 && gender == 'F') {
			System.out.println("Eligible for scholarship");
		} else {
			System.out.println("Not elgible");
		}
	}
}
