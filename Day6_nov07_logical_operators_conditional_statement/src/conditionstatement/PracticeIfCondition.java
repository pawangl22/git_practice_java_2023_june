package conditionstatement;

public class PracticeIfCondition {

	public static void main(String[] args) {
		int num = 15;

		if (num % 2 == 0) {
			System.out.println("Given number is even = " + num);
		} else {
			System.out.println("Given number is odd = " + num);
		}
	}

}
