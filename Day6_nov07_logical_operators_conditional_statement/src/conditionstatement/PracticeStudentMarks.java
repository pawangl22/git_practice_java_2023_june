package conditionstatement;

public class PracticeStudentMarks {

	public static void main(String[] args) {

		int totalMark = 35;

		if (totalMark >= 85 && totalMark >= 80) {
			System.out.println("Distionction");
		} else if (totalMark >= 75 && totalMark <= 84) {
			System.out.println("First class");
		} else if (totalMark >=45 && totalMark<=74) {
			System.out.println("Second class");
		} else {
			System.out.println("Fail");
		}
			
	}

}
