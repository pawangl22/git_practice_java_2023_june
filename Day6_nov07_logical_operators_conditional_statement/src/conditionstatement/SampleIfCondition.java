package conditionstatement;

public class SampleIfCondition {

	public static void main(String[] args) {

		int marks = 80;

		if (marks >= 75) {
			System.out.println("Distinction");
		}

	}
}
