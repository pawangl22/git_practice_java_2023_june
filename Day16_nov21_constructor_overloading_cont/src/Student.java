
public class Student {

	int slNo;
	String name;
	long phoneNo;
	int age;
	String course;

	Student(int slNo, String name, long phoneNo) {
		this.slNo = slNo;
		this.name = name;
		this.phoneNo = phoneNo;
	}

	Student(int slNo, String name, long phoneNo, int age, String course) {
		this.slNo = slNo;
		this.name = name;
		this.phoneNo = phoneNo;
		this.age = age;
		this.course = course;
	}

	// Alt+Shift+S+S -----> Overriding toString()
	@Override
	public String toString() {
		return "Studendt [slNo=" + slNo + ", name=" + name + ", phoneNo=" + phoneNo + ", age=" + age + ", course="
				+ course + "]";
	}

	public static void main(String[] args) {

		Student stu = new Student(123, "Pawan", 8879782209L);
		System.out.println(stu);
		Student stu2 = new Student(123, "Pawan", 8879782209L, 22, "BSC");
	}

}
