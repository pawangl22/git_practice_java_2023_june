public class Tshirt {
	String color;
	char size;
	double price;

	Tshirt(String color, char size, double price) {
		this(size, color, price);
	}

	Tshirt(double price, String color) {
		this('\u0000', color, price);
	}

	Tshirt(String color) {
		this(color, 'M', 500.0);
		System.out.println("hello");
	}

	Tshirt(char size, String color, double price) {
		this.size = size;
		this.color = color;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Tshirt [color=" + color + ", size=" + size + ", price=" + price + "]";
	}

	public static void main(String[] args) {
		Tshirt t = new Tshirt("Green");
		System.out.println(t);
	}

}
