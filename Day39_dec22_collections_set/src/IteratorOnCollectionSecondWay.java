import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class IteratorOnCollectionSecondWay {

	public static void main(String[] args) {
		ArrayList<String> namesList = new ArrayList<>(Arrays.asList("Alpha", "Beta", "Mark", "Gama"));

		/*Iterator<String> iterator = namesList.iterator();
		iterator.forEachRemaining(e -> {
			iterator.next();
			iterator.remove();
		});
		System.out.println(namesList); */
		
		for(String name : namesList) {
			System.out.println(name);
		} 
	}
}
