import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class SampleSet {
	public static void main(String[] args) {
		
		HashSet<String> colorSet = new HashSet<>(Arrays.asList("Red", "Green", "Blue", "Yellow", "Blue", null, null));
		System.out.println(colorSet);
		
		LinkedHashSet<String> colorSet2 = new LinkedHashSet<>(Arrays.asList("Red", "Green", "Blue", "Yellow", null, null));
		System.out.println(colorSet2);
		
		TreeSet<String> colorSet3 = new TreeSet<>(Arrays.asList("Red", "Green", "Blue", "Yellow"));
		System.out.println(colorSet3);
		
	}

}
