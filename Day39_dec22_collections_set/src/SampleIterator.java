import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class SampleIterator {
	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<>(Arrays.asList(10, 20, 40, 50, 60));

		Iterator<Integer> iterator = al.iterator();
		while (iterator.hasNext()) {
			Integer x = (Integer) iterator.next();
			System.out.println(x);
		}

	}

}
