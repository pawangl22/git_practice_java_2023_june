import java.util.Iterator;

public class SampleForLoop {

	public static void main(String[] args) {

		// int i;
		for (int i = 1; i <= 10; i++) {
			System.out.println(i);
		}
		// System.out.println("The final value is "+i);
	}
}
