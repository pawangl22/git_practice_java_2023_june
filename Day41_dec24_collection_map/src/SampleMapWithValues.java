import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class SampleMapWithValues {
	public static void main(String[] args) {
		HashMap<Integer, String> studentData = new HashMap<>();
		studentData.put(1, "Alpha");
		studentData.put(2, "Alpha");
		studentData.put(3, "Gama");
		studentData.put(4, "Charlie");
		studentData.put(5, null);
		

		Collection<String> values = studentData.values();
		for(String v : values) {
			System.out.println(v);
		}
		
		//This is sample conversion from collection to Arraylist
		ArrayList<String> sl = new ArrayList<>(values);
		System.out.println(sl);
	}
}
