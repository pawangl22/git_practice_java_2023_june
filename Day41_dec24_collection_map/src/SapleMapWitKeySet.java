import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class SapleMapWitKeySet {
	public static void main(String[] args) {
		HashMap<Integer, String> studentData = new HashMap<>();
		studentData.put(1, "Alpha");
		studentData.put(2, "Beta");
		studentData.put(3, "Gama");
		studentData.put(4, "Charlie");

		Set<Integer> keys = studentData.keySet();
		for (Integer i : keys) {
			System.out.println(i);
		}
		
		Collection<String> values = studentData.values();
		for(String v : values) {
			System.out.println(v);
		}
	}

}
