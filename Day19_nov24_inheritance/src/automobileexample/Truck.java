package automobileexample;

public class Truck extends Vehicle {

	int loadCapacity;
	
	void dropLoad() {
		System.out.println("Truck : dropping load");
	}

	@Override
	public String toString() {
		return "Truck [color=" + color + ", brand=" + brand + ", price=" + price + ", loadCapacity=" + loadCapacity
				+ "]";
	}
}
