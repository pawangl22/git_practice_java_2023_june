
package automobileexample;

public class RunnerVehicle {

	public static void main(String[] args) {

		System.out.println("***** Car Object created and printing the values *****");
		Car c = new Car();
		c.brand = "BMW";
		c.price = 6500000;
		c.isAutomatic = true;
		c.cruizeControl();

		System.out.println(c);

		System.out.println("\n***** Truck Object created and printing the values *****");
		Truck t = new Truck();
		t.brand = "Bharat";
		t.price = 89789098;
		t.loadCapacity = 5000;
		t.start();
		t.stop();
		t.dropLoad();

		System.out.println(t);

		System.out.println("\n***** Bike Object created and printing the values *****");
		Bike b = new Bike();
		b.brand = "Hero";
		b.color = "Red";
		b.inKickerAvailable = false;
		b.price = 555555;
		b.start();
		b.stop();

		System.out.println(b);

	}

}
