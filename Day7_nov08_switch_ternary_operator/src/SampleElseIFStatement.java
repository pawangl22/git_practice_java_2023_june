
public class SampleElseIFStatement {
	
	public static void main(String[] args) {
		
		String formType = "logout";
		
		if (formType.equals("login")) {
			System.out.println("User logged in");
		}
		else if (formType.equalsIgnoreCase("Logout")) {
			System.out.println("User Logged out");
		}
		else if (formType.equalsIgnoreCase("Profile")) {
			System.out.println("Profile vieweing");
		}
		else System.out.println("Invalid statement");
	}

}
