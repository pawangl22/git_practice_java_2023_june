
public class SampleSwitchStringExample3 {

	public static void main(String[] args) {

		String formType = "profile";

		switch (formType) {
		case "login":
			System.out.println("User logged in");
			break;

		case "logout":
			System.out.println("User loggd out");
			break;

		case "profile":
			System.out.println("Profile Viewing");
			break;

		default:
			System.out.println("Invalid statement");

		}
	}
}
