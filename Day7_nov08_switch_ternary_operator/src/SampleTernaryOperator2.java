
public class SampleTernaryOperator2 {

	public static void main(String[] args) {
		
		int age=-1;
		
		System.out.println((age>=0 && age<=12) ? "Child": (age>=13 && age<=100)? "Adult": "Invalid"); 
	}
}
