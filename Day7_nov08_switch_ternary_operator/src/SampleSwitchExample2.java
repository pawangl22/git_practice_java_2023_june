
public class SampleSwitchExample2 {

	public static void main(String[] args) {
		char input = 'P';

		switch (input) {
		case 'y':
		case 'Y':
			System.out.println("YES");
			break;
		case 'n', 'N':
			System.out.println("NO");
			break;
		default:
			System.out.println("Not a valid character");
		}
	}

}
