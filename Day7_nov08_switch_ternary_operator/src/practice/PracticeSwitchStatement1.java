package practice;

public class PracticeSwitchStatement1 {
	
	public static void main(String[] args) {
		
		char validationChar='P';
		
		switch(validationChar) {
		
		case 'n','N': 
			System.out.println("NO");
			break;
		
		case 'y':
		case 'Y':
			System.out.println("YES");
			break;
			
		default: System.out.println("Wrong char");
		}
	}

}
