package practice;

public class PracticeEvenOddTernary {
	
	public static void main(String[] args) {
		
		int i=6;
		System.out.println( (i%2==0) ? "EVEN" : "ODD" );
	}

}
