package practice;

public class PracticeAdultChildTernary {
	
	public static void main(String[] args) {
		
		int age=8;
		
		System.out.println( (age>=0) && (age<=17) ? "CHILD" : (age>=18) && (age<=100) ? "ADULT" : "Wrong Age" );
	}

}
