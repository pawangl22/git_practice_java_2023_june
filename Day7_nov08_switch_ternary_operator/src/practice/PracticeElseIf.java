package practice;

public class PracticeElseIf {

	public static void main(String[] args) {

		String formType = "PROFILES";

		if (formType.equalsIgnoreCase("login")) {
			System.out.println("User logged in");
			
		} else if (formType.equalsIgnoreCase("LogOut")) {
			System.out.println("User logged out");
			
		} else if (formType.equalsIgnoreCase("Profile")) {
			System.out.println("Profile is viewing");
			
		} else {
			System.out.println("Wrong page");
		}

	}

}
