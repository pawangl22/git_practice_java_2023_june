import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Logger;

public class SampleLogger {

	public static Logger log = Logger.getLogger("SampleLoggerl s");
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter two integers");
		try {
			int x = s.nextInt();
			int y = s.nextInt();
			int z = x / y;
			log.info("The result of division is "+z);
			System.out.println("The result of division is " + z);
		} catch (ArithmeticException e) {
			log.severe("Run the program again. Please change the denominator" + e);
		} catch (InputMismatchException e) {
			log.severe("Enter valid integer" + e);
		} catch (Exception e) {
			log.severe("There is some error in the program, pleae check once");
		}
		System.out.println("End of the program");

	}
}
