import java.util.InputMismatchException;
import java.util.Scanner;

public class SampleException {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter two integers");
		try {
			int x = s.nextInt();
			int y = s.nextInt();
			int z = x / y;
			System.out.println("The result of division is " + z);
		} catch (ArithmeticException e) {
			System.out.println("Run the program again. Please change the denominator" + e);
		} catch (InputMismatchException e) {
			System.out.println("Enter valid integer" + e);
		} catch (Exception e) {
			System.out.println("There is some error in the program, pleae check once");
		}
		System.out.println("End of the program");

	}
}
