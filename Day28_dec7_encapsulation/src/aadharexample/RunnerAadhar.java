package aadharexample;

public class RunnerAadhar {
	public static void main(String[] args) {
		Aadhar a = new Aadhar();
		a.setAadharNumber(8090706050L);
		a.setName("Alpha");
		a.setAge(36);
		a.setPhoneNumber(8879782209L);
		a.setAddress("BSK 2rd stage");
		System.out.println(a);
	
		Aadhar a2 = new Aadhar(9080809090L, "Pawan", 35, 8879786690L, "Katriguppe");
		System.out.println(a2);
	}

}
