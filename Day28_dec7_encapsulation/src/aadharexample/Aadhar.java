package aadharexample;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
//@AllArgsConstructor
@NoArgsConstructor
public class Aadhar {
	private long aadharNumber;
	private String name;
	private int age;
	private long phoneNumber;
	private String address;

	Aadhar(long aadharNumber, String name, int age, long phoneNumber, String address) {
		this.aadharNumber = aadharNumber;
		this.name = name;
		this.age = age;
		this.phoneNumber = phoneNumber;
		this.address = address;
	}

}
