import java.util.Scanner;

public class SquareFinder {

	static void square(int num) {
		System.out.println(num * num);
	}

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("Enter the value of n");
		int n = s.nextInt();
		SquareFinder.square(n);
		s.close();
	}
}
