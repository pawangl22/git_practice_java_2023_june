
public class Calculator {

	int add(int num1, int num2) {
		int result = num1 + num2;
		return result;
	}

	int minus(int num1, int num2) {
		int result = num1 - num2;
		return result;
	}

	int multiplication(int num1, int num2, int num3) {
		int result = num1 * num2 * num3;
		return result;
	}
	
	int divison(int num1, int num2) {
		int result = num1/num2;
		return result;
	}

	public static void main(String[] args) {
		// Creating object of the class Calculator, object reference is cal
		Calculator cal = new Calculator();

		// Calling the method add using object reference(cal) and .(dot) operator
		// and storing back the returned value in the sum variable
		// the variable name can be anything of your choice
		int sum = cal.add(5, 16);
		System.out.println("Total sum is " + sum);
		
		sum=cal.add(25, 50);
		System.out.println("Total sum is "+sum);

		int minus = cal.minus(25, 9);
		System.out.println("Final value is " + minus);

		int multi = cal.multiplication(5, 5, 5);
		System.out.println("Total value is " + multi);
		
		int division = cal.divison(10, 5);
		System.out.println("Division of num1 and num2 "+division);
	}

}
