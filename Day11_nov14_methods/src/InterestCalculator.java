import java.util.Scanner;

public class InterestCalculator {

	static double simpleInterest(int principal, int time, double rateOfInterest) {

		double si = (principal * time * rateOfInterest) / 100;
		return si;
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter principal amount");
		int pricipal = s.nextInt();

		System.out.println("Enter time");
		int time = s.nextInt();

		System.out.println("Enter rate of inerest");
		double interest = s.nextDouble();

		s.close();

		double outPut = InterestCalculator.simpleInterest(pricipal, time, interest);
		System.out.println("Simple interest value is " + outPut);

	}

}
