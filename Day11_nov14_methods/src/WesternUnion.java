import java.util.Scanner;

public class WesternUnion {

	static void convertDollarToInr(int dollar, double rate) {
		System.out.println(dollar * rate);
	}

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("Please enter dollar");
		int dollar = s.nextInt();
		System.out.println("Please enter rate");
		double rate = s.nextDouble();
		
		s.close();

		WesternUnion.convertDollarToInr(dollar, rate);
	}

}
