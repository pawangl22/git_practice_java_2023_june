import java.util.Scanner;

public class Utility {

	void greet(String name) {
		System.out.println("Hello " + name);
	}

	// Method to count the no. of characters
	static int countCharacters(String word) {
		return word.length();

	}

	static String merge(String str1, String str2) {
		return str1 + " " + str2;
	}

	static void upper(String str) {
		System.out.println(str.toUpperCase());
	}

	public static void main(String[] args) {
		Utility greeting = new Utility();
		greeting.greet("Pawan");

		Scanner s = new Scanner(System.in);
		System.out.println("Please enter a sentence");
		String sentence = s.nextLine();
		s.close();
		int length = Utility.countCharacters(sentence);
		System.out.println("The lenght of the given sentence is " + length);

		String merge = Utility.merge("Hello", "World");
		System.out.println(merge);

		Utility.upper("java program By Mahesh");

	}

}
