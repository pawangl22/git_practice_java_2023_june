
public class User {

	String name;
	String email;
	String password;

	User(String name, String email, String password) {
		this.name = name;
		this.email = email;
		this.password = password;
	}

	User(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public static void main(String[] args) {

		User user1 = new User("pawangl22@gmail.com", "123@pawan");
		System.out.println(user1.email);
		System.out.println(user1.password);
		System.out.println(user1.name);

		System.out.println("----------***********-----------\n");

		User user2 = new User("Pawan", "pawangl22@gmail.com", "123@pawan");
		System.out.println(user2.email);
		System.out.println(user2.password);
		System.out.println(user2.name);

	}

}
