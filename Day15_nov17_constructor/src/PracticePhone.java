
public class PracticePhone {
	
	static String brand;
	double price;
	String color;
	int ramSize;
	String os;
	
	PracticePhone(String brand, double price, String os) {
		this.brand=brand;
		this.price=price;
		this.os=os;
	}
	
	private PracticePhone(String brand, double price, String os, int ramSize) {
		this.brand=brand;
		this.price=price;
		this.os="IOS";
		this.ramSize=ramSize;
	}
	
	
	public static void main(String[] args) {
		PracticePhone phone1 = new PracticePhone("Apple", 45000.00, "IOS");
		phone1.ramSize=100;
		System.out.println(phone1.ramSize);
		System.out.println(phone1);
		System.out.println(phone1.brand);
		System.out.println(phone1.price);
		System.out.println(phone1.os);
		
		PracticePhone.buyPhone("Samsung", 79000.00);
		
		PracticePhone phone2 = new PracticePhone("Samsung", 99000.00, "Andriod", 12);
		System.out.println(phone2.brand);
		System.out.println(phone2.price);
		System.out.println(phone2.os);
		System.out.println(phone2.ramSize);
		
		
	}
	public static void buyPhone(String brand, double price) {
		
		System.out.println("Buying a new phone " +brand +" and price is " + price );
	}

}
