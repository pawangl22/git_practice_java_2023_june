
public class Car {

	String color;
	String brand;
	boolean isAutomatic;

	Car() {
		this.color = null;
		this.brand = "Volvo";
		this.isAutomatic = false;

	}

	public static void main(String[] args) {
		Car c = new Car();
		System.out.println(c.color);
		System.out.println(c.brand);
		System.out.println(c.isAutomatic);

	}

}
