
public class Pen {
	String color;
	
	Pen(String color){
		this.color=color;
	}

	public static void main(String[] args) {
		Pen p1 = new Pen("Blue");
		Pen p2 = new Pen("Red");
		Pen p3 = new Pen("Black");
		Pen p4 = new Pen(null);
		
		System.out.println(p1.color);
		System.out.println(p2.color);
		System.out.println(p3.color);
		System.out.println(p4.color);
		
	}
}
