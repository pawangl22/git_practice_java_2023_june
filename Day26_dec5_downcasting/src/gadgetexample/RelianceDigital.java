package gadgetexample;

import java.util.Random;

public class RelianceDigital {

	Gadget luckyDraw() {
		int option = new Random().nextInt(3) + 1;
		System.out.println("You got lucky no is " + option);
		if (option == 1) {
			Mobile m = new Mobile();
			return m;
		} else if (option == 2) {
			Laptop l = new Laptop();
			return l;
		} else if (option == 3) {
			SmartWatch s = new SmartWatch();
			return s;
		} else {
			return null;
		}
	}

	public static void main(String[] args) {

		RelianceDigital rd = new RelianceDigital();
		Gadget g = rd.luckyDraw();
		if (g != null) {
			g.turnOn();
			if (g instanceof Mobile m) {
				m.call();
			} else if (g instanceof Laptop l) {
				l.executeProgram();
			} else if (g instanceof SmartWatch s) {
				s.startTimer();
			}
			g.turnOff();
		} else {
			System.out.println("No Gift, Better luck next time!!");
		}

	}

}
