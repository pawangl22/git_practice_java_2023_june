
public class Mobile {

	String brand = "Apple";
	String color = "White";
	double price = 100000;
	int noOfCamera = 4;
	double screenSize = 6.1;

	void turnOn() {
		System.out.println("Phone turning on");
	}

	void turnOff() {
		System.out.println("Phone turning off");
	}

	void recordVideo() {
		System.out.println("Video recording is in progress");
	}

	void takePicture() {
		System.out.println("Clicking picture");
	}
	
	public static void main(String[] args) {
		
		System.out.println("************** Printing m1 objects ***************");
		
		//Creating Mobile object m1
		Mobile m1 = new Mobile();
		System.out.println(m1.brand);
		System.out.println(m1.color);
		System.out.println(m1.price);
		System.out.println(m1.noOfCamera=2);
		System.out.println(m1.screenSize=5.7);
		
		m1.turnOn();
		m1.recordVideo();
		
		System.out.println("************** Printing m2 objects ***************");
		//Creating Mobile object m2
		Mobile m2 = new Mobile();
		System.out.println(m2.brand="Samsung");
		System.out.println(m2.color="Black");
		System.out.println(m2.price=567899.00);
		System.out.println(m2.noOfCamera=4);
		System.out.println(m2.screenSize=6.5);
		
		m2.turnOn();
		m2.turnOff();
		m2.takePicture();
		m2.recordVideo();
		
	}

}
