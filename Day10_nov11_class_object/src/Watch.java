
public class Watch {
	String color = "Grey";
	String brand = "Fossil";
	double price = 29999;

	void switchOn() {
		System.out.println("Switching on the watch");
	}

	void switchOff() {
		System.out.println("Switching off the watch");
	}

	public static void main(String[] args) {
		Watch watch1 = new Watch();
		Watch watch2 = new Watch();
		Watch watch3 = new Watch();

		System.out.println("********* Printing watch1 Object **********");
		System.out.println(watch1.brand);
		System.out.println(watch1.color);
		System.out.println(watch1.price);
		watch1.switchOn();
		watch1.switchOff();

		System.out.println("********* Printing watch2 Object **********");
		System.out.println(watch2.brand = "Apple");
		System.out.println(watch2.color = "White");

		System.out.println("********* Printing watch3 Object **********");
		System.out.println(watch3.price = 35000);
		System.out.println(watch3.brand = "Samsung");
		watch3.switchOn();
		watch3.switchOff();

	}
}
